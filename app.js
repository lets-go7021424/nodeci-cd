const express=require('express');
const app=express();
const port=8080;

app.get('/',(req,res)=>{
    res.send('Node Application for CI/CD Demo!');
});

app.listen(port,()=>{
    console.log(`Server is running on port http://localhost:${port}`);
});